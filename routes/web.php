<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/game', 'GameController@index')->name('game.index');
Route::get('/game/create', 'GameController@create')->name('game.create');
Route::post('/game', 'GameController@store')->name('game.store');
Route::post('/game/delete/{id}', 'GameController@destroy')->name('game.destroy');
Route::get('/game/edit/{id}', 'GameController@edit')->name('game.edit');
Route::post('game/update/{id}', 'GameController@update')->name('game.update');
Route::get('game/detail/{id}', 'GameController@detail' )->name('game.detail');