<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;

class GameController extends Controller
{
    public function index(){
        $data_game = Game::all();
        $no = 0;
        return view('halaman.game', compact('data_game', 'no'));
    }



    public function destroy($id){
        $game = Game::find($id);
        $game->delete();
        return redirect('/game');
    }



    public function create(){
        return view('halaman.create');
    }

    public function store(Request $request){
        $game = new Game;
        $game->name      = $request->name;
        $game->gameplay  = $request->gameplay;
        $game->developer = $request->developer;
        $game->year      = $request->year;
        $game->save();
        return redirect('/game');
    }


    public function edit($id){
        $game = Game::find($id);
        return view('halaman.edit', compact('game'));

    }

    public function update(Request $request, $id){
        $game = Game::find($id);
        $game->name      = $request->name;
        $game->gameplay  = $request->gameplay;
        $game->developer = $request->developer;
        $game->year      = $request->year;
        $game->update();
        return redirect('/game');
    }

    public function detail($id){
        $game = Game::find($id);
        return view('halaman.detail', compact('game'));

    }
}