<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Table Data</title>

</head>


<body>
<div class="container.fluid">
   
<h2>List Game</h2>

<br>
<br>

<a href="{{route('game.create')}}" class="btn btn-primary mb-2 float-right mr-2">Tambah Game</a>



<table class="table">

<thead class="thead-light">

<tr>

<th scope="col">No</th>

<th scope="col">Name</th>

<th scope="col">Gameplay</th>

<th scope="col">Developer</th>

<th scope="col">Year</th>

<th scope="col" >Actions</th>

</tr>


@foreach ($data_game as $game)
<tr>
<td>{{++$no}}</td>
<td>{{$game->name}}</td>
<td>{{$game->gameplay}}</td>
<td>{{$game->developer}}</td>
<td>{{$game->year}}</td>
<td>


    <div class="row">
<form action="{{route('game.destroy', $game->id)}}" method="post">@csrf
<button  onclick="return confirm('yakin mau di hapus?')" type="submit" class="btn btn-danger mr-2">Hapus</button>
</form>

<form action="{{route('game.edit', $game->id)}}" method="get">@csrf
<button type="submit" class="btn btn-secondary mr-2">Edit</button>
</form>

<form action="{{route('game.detail', $game->id)}}" method="get">@csrf
    <button type="submit" class="btn btn-primary ">Detail</button>
    </form>

</div>
</td>
</tr>
@endforeach


</thead>

<tbody>


</tbody>

</table>
</div>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>

