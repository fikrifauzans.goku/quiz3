<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Create Data</title>

</head>

<body>
    <br><br>
    <div class="container">
<h2>Edit Data Game</h2><br><br>
{{-- {{route('update.store')}} --}}
<form action="{{route('game.update' , $game->id)}}" method="post">@csrf
<div class="mb-3">
    <label  for="exampleFormControlInput1" class="form-label">Name</label>
    <input value="{{$game->name}}" name="name" type="text" class="form-control" id="exampleFormControlInput1">
  </div>

  <div class="mb-3">
    <label for="exampleFormControlInput1" class="form-label">gameplay</label>
    <input name="gameplay" value="{{$game->gameplay}}" type="text" class="form-control" id="exampleFormControlInput1" >
  </div>

  <div class="mb-3">
    <label for="exampleFormControlInput1" class="form-label">Developer</label>
    <input value="{{$game->developer}}" name="developer" type="text" class="form-control" id="exampleFormControlInput1" >
  </div>

  <div class="mb-3">
    <label for="exampleFormControlInput1" class="form-label">Year</label>
    <input name="year" value="{{$game->Year}}" type="text" class="form-control" id="exampleFormControlInput1">
  </div>
  <button type="submit" class="btn btn-success">Update</button>

</div>

</form>




<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>

